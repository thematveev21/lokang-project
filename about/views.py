from django.shortcuts import render, redirect
from .forms import AboutEditForm
from .models import AboutDescription

# Create your views here.


def about(request):

    data = {
        'slug': '',
        'description': ''
    }

    record = AboutDescription.objects.last()
    if record:
        data['slug'] = record.slug
        data['description'] = record.description

    return render(request, 'about.html', data)


def edit_about(request):

    if request.method == 'POST':
        form = AboutEditForm(request.POST)
        if form.is_valid():

            # to db

            slug = form.cleaned_data.get('slug')
            description = form.cleaned_data.get('description')

            record = AboutDescription(
                slug=slug,
                description=description
            )

            record.save()

            print(record)

    return render(request, 'about_edit.html')
