from django.db import models

# Create your models here.


class AboutDescription(models.Model):
    slug = models.TextField()
    description = models.TextField()

    def __repr__(self):
        return f'<id:{self.id} {self.slug}>'
