from django.forms import Form, CharField


class AboutEditForm(Form):
    slug = CharField()
    description = CharField()
