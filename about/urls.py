from django.urls import path
from .views import about, edit_about

urlpatterns = [
    path('', about),
    path('edit/', edit_about)
]
